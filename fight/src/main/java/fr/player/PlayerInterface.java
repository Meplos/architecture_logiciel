package fr.player;

public interface PlayerInterface {
    public int getStrikeForce();
    public void parry(int strike_force);
    public Boolean isAlive();
    public double getHp();
}