package fr.player;

public abstract class PlayerDecorator implements PlayerInterface {
    private PlayerInterface _deco;

    public PlayerDecorator(PlayerInterface p) {
        _deco = p;
    }

    @Override
    public int getStrikeForce() {
        return _deco.getStrikeForce();
    }

    @Override
    public double getHp(){
        return _deco.getHp();
    }

    @Override
    public Boolean isAlive() {
        return _deco.isAlive();
    }

    @Override
    public void parry(int strike_force) {
        _deco.parry(strike_force);
    }

        
}