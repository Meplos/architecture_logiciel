package fr.player;

public abstract class PlayerClasses implements PlayerInterface {

    private double hp;
    private int strikeForce;
   
    @Override
    public int getStrikeForce() {
        return strikeForce;
    }

    @Override
    public Boolean isAlive() {
        return hp > 0;
    }

    @Override
    public void parry(int strike_force) {
        hp -= strike_force;
    }

    @Override
    public double getHp(){
        return hp;
    }

    public PlayerClasses(double hp, int strikeForce) {
        this.hp = hp;
        this.strikeForce = strikeForce;
    }

}