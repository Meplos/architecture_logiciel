package fr.player;

import fr.equipment.Equipment;
import fr.exception.ImpossibleExtensionException;

/**
 * UnitSimple
 */
public interface UnitSimple {
    

    public int strike();

    public void parry(int strike_force);
    
    public void addSword() throws ImpossibleExtensionException;

    public void addShield() throws ImpossibleExtensionException;

    public void addEquipment(Equipment eq) throws ImpossibleExtensionException;

	public void removeEquipment(Equipment eq);

    
}