package fr.player;

public class ShieldPlayer extends PlayerDecorator {
    

    private final int MAX_DURABILITY = 100;
    private final int MAX_SHIELD_CHARGE_SUPPORT = 100;
    private int shieldDurability = MAX_DURABILITY;
    
    @Override
    public void parry(int strike_force) {
        if(isBroken())
            super.parry(strike_force);
        else {
            if (strike_force > MAX_SHIELD_CHARGE_SUPPORT) {
                super.parry(strike_force - MAX_SHIELD_CHARGE_SUPPORT);
                shieldDurability = shieldDurability - ((20 / 100) * shieldDurability);
            } else {
                super.parry((int) Math.log(strike_force));
                shieldDurability = shieldDurability - ((10 / 100) * shieldDurability);
            }
        }

    }

    private boolean isBroken(){
        return (shieldDurability <= 0);
    }

    public ShieldPlayer(PlayerInterface p) {
        super(p);
    }

}