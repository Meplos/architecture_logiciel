package fr.player;

import fr.exception.ImpossibleExtensionException;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

import fr.equipment.*;
import fr.equipment.Equipment;

/**
 * UnitSimpleAbstract
 */
public abstract class UnitSimpleAbstract implements UnitSimple {
    private PlayerInterface soldier;
    private float nbEquipment = 0;
    private Set<Equipment> equipment;

    @Override
    public void addShield() throws ImpossibleExtensionException {
        if (nbEquipment <= 2 ){
            soldier = new ShieldPlayer(soldier);
        } else
            throw new ImpossibleExtensionException();
    }

    @Override
    public void addSword() throws ImpossibleExtensionException {
        if (nbEquipment <= 2) {
            soldier = new SwordPlayer(soldier);
            nbEquipment++;
        } else
            throw new ImpossibleExtensionException();

    }

    @Override
    public void parry(int strike_force) {
        soldier.parry(strike_force);

    }

    @Override
    public int strike() {
        return soldier.getStrikeForce();
    }

    public UnitSimpleAbstract(PlayerInterface soldier) {
        this.soldier = soldier;
        this.equipment = new HashSet<>();
    }

    public double getHp() {
        return soldier.getHp();
    }

    @Override
    public void addEquipment(Equipment eq) throws ImpossibleExtensionException {

        if (eq instanceof Sword) {
            this.addSword();
            System.out.println(this + " add sword");
            equipment.add(eq);
        }
        if (eq instanceof Shield) {
            this.addShield();
            System.out.println(this + " add shield");
            equipment.add(eq);
        }
    }

    @Override
	public void removeEquipment(Equipment eq){
        equipment.remove(eq);
        if(nbEquipment > 0)
            nbEquipment--;
    }


}