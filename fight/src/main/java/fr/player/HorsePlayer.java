package fr.player;

/**
 * HorsePlayer
 */
public class HorsePlayer extends PlayerDecorator {

    
    @Override
    public int getStrikeForce() {
        return super.getStrikeForce() + 10;
    }

    @Override
    public void parry(int strike_force) {
        super.parry(strike_force/2) ;
    }

    public HorsePlayer(PlayerInterface p) {
        super(p);
    }

    
}