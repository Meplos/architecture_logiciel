package fr.player;

import java.util.Random;

/**
 * Human
 */
public class Human extends PlayerClasses {
    private final int CRITICAL_FAILURE = 90;
    private final int CRITICAL_SUCCESS = 10;
    private final double CRITICAL_SUCCESS_PERCENT = 1.5;
    private final double CRITICAL_FAILURE_PERCENT = 0.5;

    public Human() {
        super(100,10);
    }

    @Override
    public int getStrikeForce() {
        Random rand = new Random();
        int r = rand.nextInt(100);
        if(r < CRITICAL_SUCCESS){
            return (int) (super.getStrikeForce()*CRITICAL_SUCCESS_PERCENT);
        }
        if(r > CRITICAL_FAILURE){
            return (int) (super.getStrikeForce()*CRITICAL_FAILURE_PERCENT);
        }
        else {
            return super.getStrikeForce();
        }
        
    }   


    
}