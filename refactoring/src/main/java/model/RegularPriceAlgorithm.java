package model;

class RegularPriceAlgorithm implements PriceAlgorithm {
    private final double BASE_PRICE_REGULAR = 2;
    private final int NB_DAY_REGULAR = 2;
    private final double COST_PER_DAYS_LATE_REGULAR = 1.5;
    RegularPriceAlgorithm() {

    }

    @Override
    public double getPrice(int nbDay) {
        double price = BASE_PRICE_REGULAR;
        if(nbDay > NB_DAY_REGULAR)
            price += (nbDay - NB_DAY_REGULAR) * COST_PER_DAYS_LATE_REGULAR;
        return price;
    }

    @Override
    public double getRentedPoint(int nbDay) {
        double frequentPoint = 1;
        return frequentPoint;
    }
}