package model;

public class Movie {
    public static final int CHILDRENS = 2;
    public static final int REGULAR = 0;
    public static final int NEW_RELEASE = 1;
    // get price code return price Algorithm
    private String _title;
    private int _priceCode;

    public Movie(String title, int priceCode) {
        _title = title;
        _priceCode = priceCode;
    }

    public PriceAlgorithm getPriceCode() {
        PriceAlgorithm pa;
        switch (_priceCode) {
        case CHILDRENS:
            pa = new ChildrenPriceAlgorithm();
            break;
        case REGULAR:
            pa = new RegularPriceAlgorithm();
            break;
        case NEW_RELEASE:
            pa = new NewReleasePriceAlgorithm();
            break;
        default:
            throw new RuntimeException();
        }
        return pa;
    }

    public void setPriceCode(int priceCode) {
        _priceCode = priceCode;
    }

    public String getTitle() {
        return _title;
    }

}
