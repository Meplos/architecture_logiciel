/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package model;

public class App extends Application{
    public String getGreeting() {
        return "Hello world.";
    }


    public static void main(String[] args) {
        Movie ro = new Movie("Rogue One", Movie.NEW_RELEASE);
        Movie frozen = new Movie("Reines des neiges", Movie.CHILDRENS);
        Movie sw = new Movie("Star Wars III", Movie.REGULAR);

        Rental rental_ro = new Rental(ro, 5);
        Rental rental_frozen = new Rental(frozen, 7);
        Rental rental_sw = new Rental(sw, 4);

        Customer cli = new Customer("Alexis");
        cli.addRental(rental_ro);
        cli.addRental(rental_frozen);
        cli.addRental(rental_sw);
        System.out.println(cli.statement());
        
        
    }
}
