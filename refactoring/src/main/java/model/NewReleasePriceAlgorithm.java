package model;

class NewReleasePriceAlgorithm implements PriceAlgorithm {
    private final double COST_PER_DAYS_NEW_RELEASE = 3;

    NewReleasePriceAlgorithm() {

    }

    @Override
    public double getPrice(int nbDay) {
        double price = nbDay * COST_PER_DAYS_NEW_RELEASE;
        return price;
    }

    @Override
    public double getRentedPoint(int nbDay) {
        double frequentPoint  = 1;
        if(nbDay > 1)
            frequentPoint = 2;
        return frequentPoint;
    }
}