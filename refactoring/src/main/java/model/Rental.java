package model;

public class Rental {
    private Movie _movie;
    private int _daysRented;
    private PriceAlgorithm _priceAlgorithm;
    

    public double getFrequentedPoints() {
        return _priceAlgorithm.getRentedPoint(_daysRented);
    }

    public double getPrice() {
        return _priceAlgorithm.getPrice(_daysRented);
    }

    public Rental(Movie movie, int daysRented) {
        _movie = movie;
        _daysRented = daysRented;
        _priceAlgorithm = _movie.getPriceCode();
    }

    public int get_daysRented() {
        return _daysRented;
    }

    public Movie get_movie() {
        return _movie;
    }

}

// clone
/*
 * implements clonable { clone(){ Object obj = super.clone() return (Regular)
 * obj; } }
 */