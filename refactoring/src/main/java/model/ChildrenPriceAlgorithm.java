package model;

class ChildrenPriceAlgorithm implements PriceAlgorithm {
    private final double BASE_PRICE_CHILDREN = 1.5;
    private final int NB_DAY_CHILDREN = 3;
	private final double COST_PER_DAYS_LATE_CHILDREN = 1.5;
    
    @Override
    public double getPrice(int nbDay) {
       double price = BASE_PRICE_CHILDREN;
       if(nbDay > NB_DAY_CHILDREN)
            price += (nbDay - NB_DAY_CHILDREN) * COST_PER_DAYS_LATE_CHILDREN;
        return price;
    }

    @Override
    public double getRentedPoint(int nbDay) {
        double frequentPoint = 1;
        return frequentPoint;
    }

}