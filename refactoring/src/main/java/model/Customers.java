package model;

import java.util.*;

public class Customers {
	private String _name;
	private Vector<Rental> _rentals = new Vector<>();
	
	public Customers(String name) {
		_name = name;
	}

	public void addRental(Rental rental) {
		_rentals.addElement(rental);
	}

	public String getName() {
		return _name;
	}


	/*
	@Deprecated
	public String statement() {
		double totalAmount = 0;
		int frequentRenterPoints = 0;
		Enumeration rentals = _rentals.elements();
		String result = "Rental Record for " + getName() + "\n";
		while (rentals.hasMoreElements()) {
			double thisAmount = 0;
			Rental each = (Rental) rentals.nextElement();
			switch (each.get_movie().getPriceCode()) {
            case Movie.REGULAR:
                thisAmount += BASE_PRICE_REGULAR;
                if (each.getDaysRented() > NB_DAY_REGULAR) {
                    thisAmount += (each.getDaysRented() - NB_DAY_REGULAR) * COST_PER_DAYS_LATE_REGULAR;
                }
                break;
            case Movie.NEW_RELEASE:
                thisAmount += each.getDaysRented() * COST_PER_DAYS_NEW_RELEASE;
                break;
            case Movie.CHILDRENS:
                thisAmount += BASE_PRICE_CHILDREN;
                if (each.getDaysRented() > NB_DAY_CHILDREN)
                    thisAmount += (each.getDaysRented() - NB_DAY_CHILDREN) * COST_PER_DAYS_LATE_CHILDREN;
                break;
            }
            frequentRenterPoints++;
            if ((each.get_movie().getPriceCode() == Movie.NEW_RELEASE) && (each.getDaysRented() > 1))
                frequentRenterPoints++;
            result += "\t" + each.get_movie().getTitle() + "\t" + String.valueOf(thisAmount) + " \n";
			totalAmount += thisAmount;
		}
		result += "Amount owned is " + String.valueOf(totalAmount) + "\n";
		result += "You earned " + String.valueOf(frequentRenterPoints) + " frequent renter points";
		return result;
	}
	*/

}
