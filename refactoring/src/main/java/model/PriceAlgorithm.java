package model;

interface PriceAlgorithm {
    public double getPrice(int nbDay);

    public double getRentedPoint(int nbDay);
}