package model;

import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.text.*;

/**
 * JavafxBuilder
 */
public class JavafxBuilder implements Builder {
    private Group g;

    
    @Override
    public void addSection(String s) {
        Text text = new Text();
        text.setText("\t"+ s);
        text.setFont(new Font(12));
        g.getChildren().add(text);

    }

    @Override
    public void addText(String s) {
        Text text = new Text();
        text.setText(s);
        text.setFont(new Font(12));
        g.getChildren().add(text);

    }

    @Override
    public void addTitle(String s) {
        Text title = new Text();
        title.setText(s);
        title.setFont(new Font(20));
        g.getChildren().add(title);

    }

    @Override
    public void endDoc(String s1, String s2) {
        Text text = new Text();
        text.setText(s1 + "\n" + s2);
        text.setFont(new Font(12));
        g.getChildren().add(text);

    }

    public Group getResult() {
        // TODO Auto-generated method stub
        return g;
    }

    public JavafxBuilder() {
        g = new Group();
    }

}
