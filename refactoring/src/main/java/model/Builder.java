package model;

import java.util.Vector;

public interface Builder {
    public void addTitle(String s);
    public void addSection(String s);
    public void addText(String s);
    public void endDoc(String s1, String s2);
}