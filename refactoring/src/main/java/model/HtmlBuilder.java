package model;

public class HtmlBuilder implements Builder {
    StringBuffer result = new StringBuffer();

    HtmlBuilder() {
        result.append("<! DOCTYPE html>");
        result.append("<head>");
        result.append("<title> Refactoring </title>");
        result.append("</head>");
        result.append("<body>");
    }
    @Override
    public void addSection(String s) {
        
    }

    @Override
    public void addText(String s) {
        // TODO Auto-generated method stub

    }

    @Override
    public void addTitle(String s) {
        TxtBuilder t = new TxtBuilder();
        t.addTitle(s);
        result.append("<h1>"+t.getResult()+"</h1>");

    }

    @Override
    public void endDoc(String s1, String s2) {
        // TODO Auto-generated method stub
        TxtBuilder t = new TxtBuilder();
        t.endDoc(s1,s2);
        result.append("<p>"+t.getResult()+"</p>");
        result.append("</body>");
        result.append("</html>");
    }

    public String getResult() {
        
        return result.toString();
    }

    

}
