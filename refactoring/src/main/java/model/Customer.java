package model;

import java.util.Vector;

import javafx.stage.Stage;
import javafx.scene.*;

class Customer {
    private String _name;
    private Vector<Rental> _rentals;

    public Customer(String name) {
        _name = name;
        _rentals = new Vector<>();
    }

    public String get_name() {
        return _name;
    }

    public void addRental(Rental rental) {
        _rentals.add(rental);
    }

    public double getPriceByRental(Rental rental) {
        return rental.getPrice();
    }

    public double getTotalPrice() {
        double totalPrice = 0;
        for (Rental rental : _rentals) {
            totalPrice += rental.getPrice();
        }
        return totalPrice;

    }

    public int getFrequentRenterPoints() {
        int totalFrequentPoints = 0;
        for (Rental rental : _rentals) {
            totalFrequentPoints += rental.getFrequentedPoints();
        }
        return totalFrequentPoints;

    }

    public String statement() {
        Builder b = new TxtBuilder();
        Builder jfx = new JavafxBuilder();
        statement(b);
        statement(jfx);
        Stage stage = new Stage();
        Scene scene = new Scene(((JavafxBuilder) jfx).getResult());
        stage.setScene(scene);
        stage.show();
        return ((TxtBuilder) b).getResult();
    }

    public void statement(Builder b){
       b.addTitle(get_name());
        for (Rental rental : _rentals) {
            b.addSection(rental.get_movie().getTitle());
            b.addText(String.valueOf(getPriceByRental(rental)));
        }
        b.endDoc(String.valueOf(getTotalPrice()), String.valueOf(getFrequentRenterPoints()));
    }

}