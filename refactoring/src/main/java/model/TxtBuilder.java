package model;

import java.util.Vector;

/**
 * TxtBuilder
 */
public class TxtBuilder implements Builder {

    StringBuffer result;

    public TxtBuilder() {
        result = new StringBuffer();
    }
    
    public void addTitle(String s) {
        result.append("Rental Record for "+ s +"\n");
    }

    public void addSection(String s) {
        result.append("\t" + s +"\t");
    }

    public void addText(String s){
        result.append(s+"\n");
    }

    public void endDoc(String s1, String s2) {
        result.append("Amount owned is " + s1 + "\n");
        result.append("You earned " + s2 + " frequent renter points.");
    }
    

    public String getResult() {

        return result.toString();
    }

}