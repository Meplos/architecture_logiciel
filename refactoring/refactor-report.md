# Problème de Refactoring

 + Statement trop grand
    + Calcul du prix location, un film
    + Calcul du prix ensemble de location
    + Calcul point fidélité, un film
    + Calcul point de fidélité, ensemble de location
    + Affichage des informations clients
 + Indentation
 + Gestion d'erreur dans le code
 + Classe non réutilisable
 + Magic Number
 + Sous fonction cachées
 + Nommage des variables
 + Violation de l'encapsulation